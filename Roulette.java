import java.util.Scanner;

public class Roulette {
    
    public static void main (String args[]){

        Scanner input = new Scanner(System.in);
        RouletteWheel game = new RouletteWheel();
        int startMoney = 0;

        System.out.println("Would you like to place a bet? (Y)es or (N)o");
        String desicion = input.nextLine();

        if (desicion.equals("Y")){
            System.out.println("What number would you like to bet on?");
            int betNumber = input.nextInt();
            System.out.println("How much would you like to bet?");
            int betTotal = input.nextInt();

            //spin the wheel 
            game.spin(); 

            //check is player won 
            if (game.getValue() == betNumber){
                System.out.println("Congrats! you guessed correctly.");
                int totalWon = betTotal * 35;
                int totalMoney = startMoney + totalWon;
                System.out.println("You have " + totalMoney);
            }
            else{
                System.out.println("you have guessed incorrectly");
            }
        }
    }
}
