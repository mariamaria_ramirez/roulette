import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int number;


    public void RouletteWheel(){
        random = new Random(38);
        number = 0;
    }

    public void spin() {
        int randSlot = random.nextInt();
        this.number = randSlot;
    }

    public int getValue() {
        return this.number;
    }
}